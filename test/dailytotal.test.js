const PRICE_POPCORN = 10;
const PRICE_SODA = 5;
const PRICE_COMBO = 12.5;

/**
 * Calculate totals of transactions for a convenice store that sells only 2 items - popcorn & soda.
 * The convenience store is running a special - if a customer buys both a popcorn and a soda, they get a special price for that pairing
 *
 * @param {Array.<{date: Date, item: string}>} transactions an array of transactions
 * @returns {number} the total of all transactions
 */
function calculateTotal(transactions) {
  console.log(transactions);
  throw new Error("Not Yet Implemented");
}

test("test 1 - sorted", () => {
  // arrange
  const transactions = [
    { date: new Date(2022, 1, 1), item: "soda" },
    { date: new Date(2022, 1, 2), item: "popcorn" },
    { date: new Date(2022, 1, 3), item: "soda" },
    { date: new Date(2022, 1, 3), item: "popcorn" },
    { date: new Date(2022, 1, 4), item: "popcorn" },
    { date: new Date(2022, 1, 4), item: "popcorn" },
    { date: new Date(2022, 1, 4), item: "soda" },
    { date: new Date(2022, 1, 4), item: "soda" },
    { date: new Date(2022, 1, 4), item: "soda" },
  ];

  // act
  const total = calculateTotal(transactions);

  // assert
  expect(total).toEqual(57.5);
});

test("test 3 - empty", () => {
  // arrange
  const input = [];

  // act
  const total = calculateTotal(input);

  // assert
  expect(total).toEqual(0);
});
